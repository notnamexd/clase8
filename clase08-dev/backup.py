# Función para cargar los datos de un archivo CSV
def cargar_datos_csv(archivo):
    datos = []
    with open(archivo, 'r') as file:
        for line in file:
            datos.append(line.strip().split('|' if archivo.endswith('.dat') else ','))
    return datos

# Cargar datos de los archivos
fechas = cargar_datos_csv('data/fechas.dat')
delitos_entrada = cargar_datos_csv('delitos_entrada2021.csv')


# Crear un diccionario para mapear fechas a IDs
mapeo_fechas_ids = {}
for fecha in fechas:
    mapeo_fechas_ids[fecha[1]] = fecha[0]

# Variable para el ID autoincrementado
id_autoincrementado = 1

# Crear un archivo nuevo para guardar las correspondencias
with open('delitos_actualizados.csv', 'w') as outfile:
    outfile.write('"id"|"fecha_id"|"franja"\n')
    for delito in delitos_entrada:
        fecha_delito = delito[4]
        for fecha in fechas:
            if fecha_delito == fecha[1]:
                fecha_id = fecha[0]
                franja = delito[5]
                outfile.write(f"{id_autoincrementado}|{fecha_id}|{franja}\n")
                id_autoincrementado += 1
                break

print("Se ha creado el archivo 'delitos_actualizados.csv' con las correspondencias de fechas, IDs y franjas.")
