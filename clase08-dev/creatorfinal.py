# Función para cargar los datos de un archivo CSV
def cargar_datos_csv(archivo):
    datos = []
    flag = 0
    with open(archivo, 'r') as file:
        for line in file:
            datos.append(line.strip().split('|' if archivo.endswith('.dat') else ','))
    return datos

# Cargar datos de los archivos
fechas = cargar_datos_csv('data/fechas.dat')
delitos_entrada = cargar_datos_csv('delitos_entrada2021.csv')
delitos = cargar_datos_csv('data/delitos.dat')
subdelitos = cargar_datos_csv('data/subdelitos.dat')
barrios = cargar_datos_csv('data/barrios.dat')

# Crear un diccionario para mapear fechas a IDs
#mapeo_fechas_ids = {}
#for fecha in fechas:
 #   mapeo_fechas_ids[fecha[1]] = fecha[0]

# Variable para el ID autoincrementado
id_autoincrementado = 1

# Crear un archivo nuevo para guardar las correspondencias
with open('delitos_actualizados.csv', 'w') as outfile:
    outfile.write('"id"|"fecha_id"|"franja"|"id_delito"|"id_subdelito"|"uso_arma"|"uso_moto"|"id_barrio"|"lat"|"long"|"cant"\n')
    for datos in delitos_entrada:
        fecha_delito = datos[4]
        tipo_delito = datos[6]
        subtipo_delito = datos[7]
        barrio_delito = datos[10]
        franja = datos[5]
        uso_arma = datos[8]
        uso_moto = datos[9]
        lat = datos[12]
        long = datos[13]
        cant = datos[14]
       
        fecha_id = ""
        delito_id = ""
        subdelito_id = ""
        barrio_id = ""
        
        for fecha in fechas:
            if fecha_delito == fecha[1]:
                fecha_id = fecha[0]
                break 
        
        for delito in delitos:
            if tipo_delito == delito[1]:
                delito_id = delito[0]
                break 
        
        for subdelito in subdelitos:
            if subtipo_delito == subdelito[1]:
                subdelito_id = subdelito[0]
                break 
        
        for barrio in barrios:
            if barrio_delito == barrio[1]:
                barrio_id = barrio[0]
                break 
        
        outfile.write(f"{id_autoincrementado}|{fecha_id}|{franja}|{delito_id}|{subdelito_id}|{uso_arma}|{uso_moto}|{barrio_id}|{lat}|{long}|{cant}\n")       
                
        id_autoincrementado += 1

print("Se ha creado el archivo 'delitos_actualizados.csv' con las correspondencias de fechas, IDs y franjas.")

