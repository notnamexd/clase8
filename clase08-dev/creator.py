import os.path

if os.path.isfile("data/fechas.dat"):
    os.remove("data/fechas.dat")

if os.path.isfile("data/barrios.dat"):
    os.remove("data/barrios.dat")

if os.path.isfile("data/delitos.dat"):
    os.remove("data/delitos.dat")

if os.path.isfile("data/subdelitos.dat"):
    os.remove("data/subdelitos.dat")

try:
    with open("delitos_entrada2021.csv", "r") as file:
        lines = file.readlines()[1:]  # Empezar desde la segunda línea
        with open("data/fechas.dat", "a+") as fecha, open("data/barrios.dat", "a+") as barrios, open("data/delitos.dat", "a+") as delitos, open("data/subdelitos.dat", "a+") as subdelitos:
            fecha.write("id|fecha\n")
            barrios.write("id|barrio_1|barrio_2\n")
            delitos.write("id|delito\n")
            subdelitos.write("id|subdelito\n")

            codfecha = 1
            codbarrio = 1
            coddelito = 1
            codsubdelito = 1
            fechas_written = set()  # Conjunto para almacenar los índices de fechas ya escritos
            barrios_written = set()  # Conjunto para almacenar los índices de barrios ya escritos
            delitos_written = set()  # Conjunto para almacenar los índices de delitos ya escritos
            subdelitos_written = set()  # Conjunto para almacenar los índices de subdelitos ya escritos
            for line in lines:
                datafile = line.split(",")
                fecha_data = datafile[4].strip()
                barrio_data_1 = datafile[10].strip()  # Índice 10
                barrio_data_2 = datafile[11].strip()  # Índice 11
                delito_data = datafile[6].strip()  # Índice 6
                subdelito_data = datafile[7].strip()  # Índice 7
                
                # Guardar en fechas.dat y barrios.dat en cada iteración
                if fecha_data not in fechas_written:
                    fecha.write(str(codfecha).zfill(3) + '|' + fecha_data + '\n')
                    fechas_written.add(fecha_data)
                    codfecha += 1
                if (barrio_data_1, barrio_data_2) not in barrios_written:
                    barrios.write(str(codbarrio).zfill(2) + '|' + barrio_data_1 + '|' + barrio_data_2 + '\n')
                    barrios_written.add((barrio_data_1, barrio_data_2))
                    codbarrio += 1
                
                # Guardar datos en delitos.csv solo si el dato no ha sido escrito antes
                if delito_data not in delitos_written:
                    delitos.write(str(coddelito).zfill(2) + '|' + delito_data + '\n')
                    delitos_written.add(delito_data)
                    coddelito += 1
                
                # Guardar datos en subdelitos.csv solo si el dato no ha sido escrito antes
                if subdelito_data not in subdelitos_written:
                    subdelitos.write(str(codsubdelito).zfill(2) + '|' + subdelito_data + '\n')
                    subdelitos_written.add(subdelito_data)
                    codsubdelito += 1
except FileNotFoundError:
    print("No se puede continuar, error en la apertura de archivos")
    exit()
